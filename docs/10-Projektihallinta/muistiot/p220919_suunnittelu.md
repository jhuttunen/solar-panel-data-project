Viikkorytmi muistuttaa sprinttien tekoa. Viikko alkaa suunnittelulla, jossa katsotaan tehtävät sprintille. Viikon aikana pidetään ainakin yksi yhteinen palaveri, jossa keskustellaan tehtävien etenemisestä. Perjantaisin tehdään viikon yhteenveto.

Tällä viikolla:
- Samu tekee dokumentointia projektihallinnasta, tehtävän 2 koontia loppuviikosta, työnjako seuraavalle tehtävälle
- Teemu tutkii dataa ja tekee visualisointia (loppuviikosta)
- Joona visualisoi hyötysuhdetta
- Jere visualisoi hinta- ja tehokehitystä
- Risto tutkii inverttereiden osuutta asiaan (tehtävänanto: Minkä kokoisia inverttereitä ajetaan ylikuormalla?)

Listaukseen saatetaan tehdä tarvittaessa muutoksia, esimerkiksi perustuen priorisointiin.

PS. Tein pari lisäystä palaverin jälkeen.
