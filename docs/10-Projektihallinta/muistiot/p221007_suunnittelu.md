## Palaverien aikataulu 10.-14.10.

- ma klo 10
- ke klo 10
- pe klo 10

### Mikä muuttui?

- Juhan palaverissa viikon "tulokset" ja seuraavan viikon suunnitelman esittely
  - 10.10. poikkeuksellisesti 2 viikon suunnitelma (eli tämä)
- Maanantaisin ei toista suunnittelupalaveria
- ke lyhyt tilannekatsaus
- pe lyhyt tilannekatsaus ja suunnittelu seuraavalle viikolle

Aikataulutettujen palaverien lisäksi voi olla hyvä yrittää ratkaista ilmeneviä ongelmia porukalla.

## Suunnitelma

- Pitäisi esikäsitellä paneelinvalmistajat (3 kpl) ja asentajat, jotka on datassa niminä
  - paneelinvalmistajasarakkeiden yhdistäminen?
- Ei liian monimutkaista arviointia system_size_DC paikkansapitävyydestä, koska datassa jonkin verran puutteita ja epäjohdonmukaisuutta
- Luokittelu RES, small NON-RES ja large NON-RES?
  - Kysytään selvennystä tehtävänantoon, jossa puhutaan kuluttaja- ja yritysasiakkaista
- Valmistajien asennusten koot??? (ei välttämätön)
  - Voi olla vaikea eritellä, jos yhdessä asennuksessa on useampia valmistajia
  - Vai koot asentajan mukaan?

- Minkälaisia visualisointeja
  - system size trend
  - installed price trend (Stand alone vs. Paired)(RES ja muita ryhmiä?)
  - CA alueellinen paneelien kumuloituminen (kartta)
  - miten visualisoida asennusfirmojen ja valmistajien aloitusvuodet?
    - testataan eri vaihtoehtoja
  - Teknologioiden vertailu
    - sarakkeet
      - technology_module_1_2_3
      - bifacial_module_1_2_3 ??
      - module_model_1_2_3 voi olla jo liian yksityiskohtainen tarkastelu
      - BIPV_module_1_2_3 voi miettiä erillisenä tarkasteluna, jos haluaa
    - hyötysuhse?
    - hinta/teho?
    - yleisyys
