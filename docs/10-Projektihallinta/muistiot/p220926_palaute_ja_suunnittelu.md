## Aamupalaute

Aiheesta Tehtävä 2

- Koodilohkojen otsikot paremmin Markdown-soluhin
- Kuvaajatesti (maalikkotesti), onko kuvaaja tarpeeksi selkeä
- Taustatyöt, pieni selitys, mitä linkkien takana on
- Selitykset tai kuvaajien tulkinta tekstinä, lisäksi voi laittaa pääkohdat ranskalaisilla viivoilla
  - Pitkiin selityksiin väliotsikoita?

## Iltapäiväsuunnittelu

- Merkataan tekemisen aiheet gitlab issueen
  - yhdessä issuessa voi olla enemmän kuin yksi tekijä, jolloin tehdään tiiviimpää yhteistyötä
- new_construction yhteys data_provider_2 sarakkeeseen ?
- Ke palaverille aika? Klo 10, koska klo 14:30 vaiheen 2 katselmointi
