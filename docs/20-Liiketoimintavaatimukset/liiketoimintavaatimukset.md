# Liiketoimintavaatimukset

Tässä käydään läpi projektin tehtävänanto tutkien sen liiketoiminnan kannalta olellisia asioita.

Projektin on tarkoitus jäljitellä oikeaa dataprojektia, joka tehdään yritykselle kuunnellen sen toiveita. Tällä kertaa yritystä edustaa kurssin opettaja.

>## Tehtävänanto
>
>- Yleisin asennettujen järjestelmien koko eri vuosina. Tähän liittyen tulisi selvittää hinta ja järjestelmäkoko kuluttaja- ja yritysasiakkaiden osalta asennusvuosittain (ts. millaisia järjestelmiä kunakin vuonna on asennettu näille asiakastyypeille)?
>- Asennettujen järjestelmien hintakehitys vuosien varrella?
>- Minkä kokoisia inverttereitä ajetaan ylikuormalla?
>- Yritys- ja kuluttaja-asiakkaiden osuudet osavaltioittain tai kaupungeittain?
>- Kuinka pitkään paneelivalmistajat ja asennusfirmat ovat toimineet alalla?
>  - Optio: Näistä lisäksi myyntimäärät ja myyntimäärien kehitys vuosien saatossa.
>- Millä aurinkopaneelijärjestelmillä on paras hinta-teho-suhde?
>- Optio: Kaupunkien ryhmittely suuremmiksi alueiksi (kuitenkin pienemmät alueet kuin osavaltiot) (vinkki: Rest APIn hyödyntäminen).

## Miten yritys voisi hyötyä tehtävänannon mukaisesta data-analyysistä?

- Myyntitilastojen seuranta on tietysti myyjän ja valmistajan kannalta tärkeää, jotta voidaan arvioida kysynnän kehitystä.
- Alueellisella tarkastelulla voidaan arvioida uusien alueiden markkinoiden potentiaalia. Alueden eroavaisuudet esim. politiikassa eivät välttämättä selviä kuitenkaan datasta.
