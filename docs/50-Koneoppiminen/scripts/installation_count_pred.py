import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# Source: https://towardsdatascience.com/the-complete-guide-to-time-series-forecasting-using-sklearn-pandas-and-numpy-7694c90e45c1
def window_input_output(input_length: int, output_length: int, data: pd.DataFrame) -> pd.DataFrame:
    
  df = data.copy()
    
  i = 1
  while i < input_length:
    df[f'x_{i}'] = df['x_0'].shift(-i)
    i = i + 1
        
  j = 0
  while j < output_length:
    df[f'y_{j}'] = df['x_0'].shift(-input_length-j)
    j = j + 1
        
  df = df.dropna(axis=0)
    
  return df


def seq_train_test_split(input_length: int, output_length: int, data: pd.DataFrame):
  seq_df = window_input_output(input_length, output_length, data)
  N = len(seq_df)

  test_size = 1
  train_size = N - output_length

  X_cols = [col for col in seq_df.columns if col.startswith('x_')]
  y_cols = [col for col in seq_df.columns if col.startswith('y_')]

  X_train = seq_df[X_cols][:train_size].values
  y_train = seq_df[y_cols][:train_size].values

  X_test = seq_df[X_cols][-test_size:].values
  y_test = seq_df[y_cols][-test_size:].values

  return X_train, X_test, y_train, y_test, test_size, N


def get_timeserie(df, freq, time_format):
  # Lasketaan asennukset
  df['time_step'] = df['installation_date'].dt.strftime(time_format)
  df = df['time_step'].value_counts().sort_index()
  df = pd.DataFrame({'time_step': df.index, 'count': df})

  # Tavoiteltava indeksi
  index_period = pd.period_range(start='1998-01-01', end='2019-12-29', freq=freq).strftime(time_format)
  period = pd.DataFrame(data=index_period, columns=['time_step'], dtype='object')

  # Yhdistetään ja lisätään nollat
  df_time_serie = period.merge(df, how='left', on='time_step')
  df_time_serie.fillna(0, inplace=True)
  df_time_serie.index = index_period
  df_time_serie.drop(['time_step'], axis=1, inplace=True)

  return df_time_serie


def get_df_ML(df, start, end, freq, time_format, r_periods=5):
  min_periods = int(r_periods / 2 + 1)
  df['rolling_count'] = df['count'].rolling(r_periods, min_periods=min_periods, center=True).mean()
  # Rajaus
  a = datetime.fromisoformat(start).strftime(time_format)
  b = datetime.fromisoformat(end).strftime(time_format)
  df = df.loc[a:b,:]

  # Piirto

  # Varaudutaan käyttämään myös alueen ylittävää päiväystä kuvassa
  x_ticks_range = pd.period_range(start=start, end='2023-12-01', freq=freq).strftime(time_format)
  N = len(df)
  def format_date(x, pos=None):
    thisind = np.clip(int(x + 0.5), 0, N - 1)
    #return df.index[thisind]
    return x_ticks_range[thisind]

  fig, ax = plt.subplots(figsize=(14, 5))
  ax.plot(df['count'], label='count')
  #ax.plot(df['ewm_count'], label='ewm')
  ax.plot(df['rolling_count'],  label='rolling')
  if N > 24:
    locator = mdates.AutoDateLocator(minticks=6, maxticks=16)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(format_date)
  else:
    fig.autofmt_xdate()
  ax.legend()
  ax.grid()
  plt.show()

  pd.set_option("mode.chained_assignment", None)

  df['x_0'] = df['rolling_count']
  #df.drop(['count', 'ewm_count', 'rolling_count'], axis=1, inplace=True)
  df.drop(['count', 'rolling_count'], axis=1, inplace=True)

  return df
