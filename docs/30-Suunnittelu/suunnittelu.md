## Esikäsittely - ideoita

#### Esikäsittelyjen tekeminen sarakkeittain?

- Käsittely aina samalla tavalla riippumatta käytetäänkö data-analyysiin vai koneoppimiseen
- Valitaanko osa sarakkeista
- ei suodateta tai vähennetä rivien määrää
- suodatuksia joudutaan tekemään erikseen vakioesikäsittelyn jälkeen?

#### Uusien sarakkeiden tekeminen

- Luokittelu?
- $/kW omaksi sarakkeeksi?
- muuta?

#### Vakioesikäsittely

- Tarkoitus ladata data pandas.DataFrame muotoon
- Puuttuvien arvojen käsittely
- Poikkeamien eliminointi vai muuttaminen johonkin toiseen arvoon?
- Toisen arvon määritystarve epätodennäköinen

#### Sarakkeet jotka tarvitsevat enemmän huomiota kuin `na_values = -9999`

- total_installed_price
- installation_date
- city
- zip_code
- installer_name
- inverter_loading_ratio ?

#### Periaatetta esikäsittelyyn

- Helppo hyödyntää jatkossa (mergellä mainiin)
- Siististi
- Lyhyesti (jos mahdollista)
- Vaiheet perusteltu tarvittaessa
- Toimivuus testattu

## Muuta

- Tarkastuslaskelmia? (datan tutkimista)
  - Onko puuttuvien system_size_DC arvojen laskenta mahdollista? (module_quantity * nameplate_capacity_module)
  - Onko syytä (tai edes mahdollista) tarkastaa system_size_DC arvot (module_quantity * nameplate_capacity_module) laskuilla?
  - Onko inverter_loading_ratio laskettavissa (system_size_DC / output_capacity_inverter) ja tarvitseeko huomioida myös inverter_quantity?
- Tulisiko tutkia dataa sen toimittajan perusteella ? (data_provider) (vaikuttaa melko turhalta, mutta saattaa paljastaa jotain muusta datasta)

#### total_installed_price riippuvuus sarakkeista

Tutkitaanko mahdollisia yhteyksiä hintaan seuraavien sarakkeiden kohdalla? (Vaikka käytännön hyötyjä on vaikea arvioida datan perusteella)

- rebate_or_grant
- third_party_owned
- self_installed
- technology_module
- bifacial_module
- module_manufacturer
- inverter_manufacturer
- DC_optimizer
- solar_storage_hybrid_inverter
- battery_rated_capacity_kWh
- battery_rated_capacity_kW

## Koneoppiminen

- Luokittelun testaus
- Klusterointi
- Mahdollisia kokeiluja
  - Oppiiko päättelemään RES-asennuksen hinnan ja tehon perusteella, mitä komponentteja tai paneeliteknologiaa on käytetty, vaikkapa yhden vuoden aineistolla, johon on tehty erilaisia suodatuksia
  - Oppiiko päättelemään RES-asennuksen hinnan ja tehon perusteella, mihin kaupunkiin asennus on tehty