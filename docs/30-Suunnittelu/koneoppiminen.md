# Koneoppimissuunnitelmat

Tässä muistiossa on ensin mietitty ideoita, joita kehiteltiin koneoppimisvaiheen alussa. Osaa ideoista lähdettiin toteuttamaan sellaisenaan. Ensimmäisten toteutusten jälkeen ideat ovat toisaalta vaihtuneet toisiin tai jalostuneet.

Jäljempänä on projektiin valikoituneet toteutettavat koneoppimisaiheet tiivistetysti.

## Ideointi

Muutamia ideoita koneoppimismalleihin.

### Menetelmiä

- Luokittelun testaus
  - kategoristen muuttujien käyttö
    - karsintaa yleisimpiin kategorioihin
      - esim. vain yleisimpiin asentajiin/valmistajiin
- Klusterointi
  - poikkeamien haku
    - esikäsittelyn jatkona

### Mahdollisia kokeiluja

- puuttuvien arvojen arviointi
  - testaus kokeilemalla osaako kone päätellä jonkin sarakkeen arvon, kun se on ensin poistettu
- Oppiiko päättelemään RES-asennuksen hinnan ja tehon perusteella, mitä komponentteja tai paneeliteknologiaa on käytetty, vaikkapa yhden vuoden aineistolla, johon on tehty erilaisia suodatuksia
- Oppiiko päättelemään RES-asennuksen hinnan ja tehon perusteella, mihin kaupunkiin asennus on tehty
  - ei ehkä paras idea

- klusterointi
  - akkujärjestelmien tunnistaminen

- asennusten saturaatio alueellisesti
  - markkinan potentiaali
  - kysynnän kehityksen arviointi

## Toteutettavat

Tässä kappaleessa on koneoppimisvaiheessa tutkittavat aiheet pääpiirteittäin.

### Akkujärjestelmien luokittelu

Tutkitaan onnistuuko järjestelmän luokittelu akulliseksi tai akuttomaksi pelkän hinnan [$/W] ja asentajan perusteella.

**Käytettävät mallit:**  
- Gaussian Naive Bayes
- Complement Naive Bayes

**Optio:**  
- Kokeillaan myös muiden piirteiden käyttöä samoille malleille.

### Asennusmäärien ja järjestelmäkoon ennustaminen

Muokataan datasta aikasarja, jossa on laskettu asennusten määrät viikoittain, kuukausittain tai vuosittain. Valitaan tästä datasta eri mittaisia syötteitä mallille, joka ennustaa syötteen perusteella määrätyn määrän aikasarjan seuraavia datapisteitä.

**Käytettävät mallit:**  
- RandomForestRegressor (ainakin jos tekee suoraan multioutput ennusteen)
- DecisionTreeRegressor

**Optio:**  
Voiko tähän yhdistää alueellisen tarkastelun? Onnistuisiko asennusten laskeminen aikasarjamuotoon vain tietyltä alueelta, jolloin ennuste koskisi vain kyseistä aluetta?

Edellisten lisäksi asennusmäärän asemasta voidaan laskea ennuste jollekin muulle ominaisuudelle, kuten järjestelmän sähköteholle tai paneelin hyötysuhteelle.

### Järjestelmän laadun luokittelu

#### Pisteytetään joitakin ominaisuuksia 
* $/kWh x eficeny käytetään luokitteluun cut metodilla 
* Tehdään malli, joka luokittelee järjestelmiä kyseisiin luokkiin.
* Mallissa käytetään tällähetkellä: manufacturer, model, system size DC ominaisuuksia
  * Ajatus on, että ostajana tai jällenmyyjänä voisin syöttää nuo arvot ja saisin tulokseksi parhaat mahdolliset tai...
  * Jos asiakkan lompakko ei ole niin paksu niin hinnnasta tinkimällä olisi mahdollista katso huonompia vaihtoehtoja
* Mahdollisesti myös selvitetään parhaimpien luokkien valmistajat ja mallit tai mahdollisesti jopa luokka kohtaisesti
* Luokittelu asteikko 1-3 tai vaihtoehtoisesti kaikki voisi yrittää luokitella järjestykseen jos mahdollista (asiaa tutkitaan vielä)
* Tämä malli ei tietenkään ota huomioon kaikke esim. 
  * Minkä valmistajan laitteet ovat kestävimpiä
  * Onko tyyppi vikoja
  * Löytyykö kyseisen myyjän tukkurilta tiettyä laitetta... ym.

**Käytettävä malli:**  
- RandomForestClassifier

### Asennusten maantieteellinen klusterointi
* Pyritään luomaan asennusten määrien ja sijaintien perusteella maantieteellisiä klustereita ja visualisoimaan näitä kartalla.

**Käytettävä malli DBSCAN**
* Halutaan luoda klustereita nimenomaan asennusten määrien perusteella
* Klusterien määrää ei haluta etukäteen asettaa, kuten monissa muissa malleissa tehdään (esim. Kmeans)
* Kaikkia asennuksia ei haluta sisällyttää klustereihin, vaan malli karsii poikkeavia arvoja
* Näkyvä tulos menetelmän ajamiseen käytettyyn aikaan suhteutettuna vaikuttaa tarkoitukseen sopivalta verrattuna muihin (OPTICS, HDSCAN) vastaavalla periaattella kokeiltuihin 

**Pohdittavaa jatkossa**
* Onko tällaisen klusteroinnin tuloksia mitenkään mahdollista konkreettisesti arvioida muuten kuin visuaalisesti?
* Antaako klusteroinnin käyttäminen mitä lisäarvoa? Sinälläänhän tässä vain luodaan tiheitä alueita, jotka ovat oikeastaan jo olemassa datassa kaupunkien muodossa.
* Millä perusteella klusteroinnissa käytettäviä parametreja (dbscanissa eps ja min_samples) olisi mahdollista säätää optimaalisimmiksi?
* Olisiko mahdollista painottaa tuloksia esim. asennuksen hinnan/koon perusteella (sample_weights parametri), jolloin arvokkaammat asennukset nostaisivat jotenkin klusterissa arvoa?