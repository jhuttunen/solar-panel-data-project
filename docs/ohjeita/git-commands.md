## git branch

Haarojen listaus  
`$ git branch`

Luodaan uusi  
`$ git branch <branch>`

Siirtyminen haaraan  
`$ git checkout <branch>`

Haaran lisääminen (ja push) remoteen/palvelimelle (origin == oletus remote)  
`$ git push --set-upstream origin <branch>`

Haaran poisto  
`$ git branch -d <branch>`

Katso commitit ja branchit erillisessä ikkunassa graafisesti  
`$ gitk`

## Remotessa olevan haaran lataus
https://github.com/k88hudson/git-flight-rules#i-want-to-checkout-to-a-remote-branch-that-someone-else-is-working-on

Haetaan kaikki haarat remotelta  
`$ git fetch --all`

Siirrytään ensimmäistä kertaa haaraan  
`$ git checkout --track origin/<branch>`

## git reset
https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified

Muutetaan kaikki 'staged' -> 'unstaged' (ei muuta tiedostoja)  
`$ git reset` tai `$ git reset --mixed`

Kumotaan commit, ilman tiedostojen muuttamista (ja vaihdetaan tiedostot 'staged')  
`$ git reset --soft HEAD~`

Kumotaan 2 committia, ilman tiedostojen muuttamista  
`$ git reset --soft HEAD~2`

Kumotaan commit ja poistetaan muutokset (mahdollisesti vaarallinen)  
`$ git reset --hard HEAD~`

## git branch + git reset

Siirretään 2 jo tehtyä committia uuteen haaraan  
```
$ git branch <branch>
$ git reset --hard HEAD~2
$ git checkout <branch>
```

## git rebase

Voi siirtää haaran lähtemään eri kohdasta. Toisin sanoen lisätään alkuperäiseen haaraan tulleita committeja. Älä tee rebase haaralle, joka on pushattu johonkin remoteen.  
```
$ git checkout <branch>
$ git rebase main
```

## Kuinka tehdä commit main haaraan, kun työ omassa haarassa on kesken

Ennen haaran vaihtoa on tehtävä commit joko väliaikaisena tai "pysyvänä".

Väliaikainen  
`$ git commit -a -m "WIP"` (WIP eli work in progress)

Pysyvä(mpi)  
`$ git commit -a -m "Hyvä commit msg"`

Sitten voidaan vaihtaa main haaraan  
`$ git checkout main`

Tarkistetaan, että main on ajan tasalla esim. näin  
`$ git pull` tai `git log -1`

Ei tehdä commit suoraan main haaraan, joten ensin tehdään uusi  
`$ git branch <branch>`  
`$ git checkout <branch>`

Sitten tehdään koodaus. Ja siitä commit. Kun on valmista, haara palvelimelle  
`$ git push --set-upstream origin <branch>`

Merget main haaraan tehdään palvelimella (tässä projektissa). `Merge request` voi tehdä valmiiksi.

Palaaminen takaisin omaan haaraan  
`$ git checkout <oma branch>`

Jos teit väliaikaisen commit, sen voi kumota näin (ei muuta tiedostoja)  
`$ git reset --mixed HEAD~`

**HUOM.** On todennäköistä, että haarojen välinen copy-paste (ainakin kokonaan localissa) osoittautuu nopeasti harmilliseksi puuhaksi.

## git log
https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History

Tällä tutkitaan commithistoriaa komentoriviltä. Itse käytän usein seuraavia komentoja.

Tarkastetaan 2 viimeisintä nykyisen haaran committia  
`$ git log -2`

Tarkastetaan viimeisimmän commitin muutokset  
`$ git log -p -1`

Tarkastetaan 5 viimeisimmän commitin tiedostot, joita on muutettu  
`$ git log -5 --stat`
