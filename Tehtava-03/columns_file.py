azimuth = ['azimuth_1', 'azimuth_2', 'azimuth_3']
tilt = ['tilt_1', 'tilt_2', 'tilt_3']
module_manufacturer = ['module_manufacturer_1', 'module_manufacturer_2', 'module_manufacturer_3']
module_model = ['module_model_1', 'module_model_2', 'module_model_3']
module_quantity = ['module_quantity_1', 'module_quantity_2', 'module_quantity_3']
technology_module = ['technology_module_1', 'technology_module_2', 'technology_module_3']
BIPV_module = ['BIPV_module_1', 'BIPV_module_2', 'BIPV_module_3']
bifacial_module = ['bifacial_module_1', 'bifacial_module_2', 'bifacial_module_3']
nameplate_capacity_module = ['nameplate_capacity_module_1', 'nameplate_capacity_module_2', 'nameplate_capacity_module_3']
efficiency_module = ['efficiency_module_1', 'efficiency_module_2', 'efficiency_module_3']
inverter_manufacturer = ['inverter_manufacturer_1', 'inverter_manufacturer_2', 'inverter_manufacturer_3']
inverter_model = ['inverter_model_1', 'inverter_model_2', 'inverter_model_3']
inverter_quantity = ['inverter_quantity_1', 'inverter_quantity_2', 'inverter_quantity_3']
micro_inverter = ['micro_inverter_1', 'micro_inverter_2', 'micro_inverter_3']
solar_storage_hybrid_inverter = ['solar_storage_hybrid_inverter_1', 'solar_storage_hybrid_inverter_2', 'solar_storage_hybrid_inverter_3']
built_in_meter_inverter = ['built_in_meter_inverter_1', 'built_in_meter_inverter_2', 'built_in_meter_inverter_3']
output_capacity_inverter = ['output_capacity_inverter_1', 'output_capacity_inverter_2', 'output_capacity_inverter_3']

end_1of3 = ['azimuth_1', 'tilt_1', 'module_manufacturer_1', 'module_model_1', 'module_quantity_1', 'technology_module_1', 
            'BIPV_module_1', 'bifacial_module_1', 'nameplate_capacity_module_1', 'efficiency_module_1', 
            'inverter_manufacturer_1', 'inverter_model_1', 'inverter_quantity_1', 'micro_inverter_1', 
            'solar_storage_hybrid_inverter_1', 'built_in_meter_inverter_1', 'output_capacity_inverter_1']

end_2of3 = ['azimuth_2', 'tilt_2', 'module_manufacturer_2', 'module_model_2', 'module_quantity_2', 'technology_module_2', 
            'BIPV_module_2', 'bifacial_module_2', 'nameplate_capacity_module_2', 'efficiency_module_2', 
            'inverter_manufacturer_2', 'inverter_model_2', 'inverter_quantity_2', 'micro_inverter_2', 
            'solar_storage_hybrid_inverter_2', 'built_in_meter_inverter_2', 'output_capacity_inverter_2']

end_3of3 = ['azimuth_3', 'tilt_3', 'module_manufacturer_3', 'module_model_3', 'module_quantity_3', 'technology_module_3', 
            'BIPV_module_3', 'bifacial_module_3', 'nameplate_capacity_module_3', 'efficiency_module_3', 
            'inverter_manufacturer_3', 'inverter_model_3', 'inverter_quantity_3', 'micro_inverter_3', 
            'solar_storage_hybrid_inverter_3', 'built_in_meter_inverter_3', 'output_capacity_inverter_3']

data_provider = ['data_provider_1', 'data_provider_2']
system_ID = ['system_ID_1', 'system_ID_2']
installation_date = ['installation_date']
system_size_DC = ['system_size_DC']
total_installed_price = ['total_installed_price']
rebate_or_grant = ['rebate_or_grant']
customer_segment = ['customer_segment']
expansion_system = ['expansion_system']
multiple_phase_system = ['multiple_phase_system']
new_construction = ['new_construction']
tracking = ['tracking']
ground_mounted = ['ground_mounted']
zip_code = ['zip_code']
city = ['city']
state = ['state']
utility_service_territory = ['utility_service_territory']
third_party_owned = ['third_party_owned']
installer_name =  ['installer_name']
self_installed = ['self_installed']
additional_modules = ['additional_modules']
additional_inverters = ['additional_inverters']
DC_optimizer = ['DC_optimizer']
inverter_loading_ratio = ['inverter_loading_ratio']
battery_rated_capacity_kW = ['battery_rated_capacity_kW']
battery_rated_capacity_kWh = ['battery_rated_capacity_kWh']


def all_orig():
  all_orig = []
  all_orig.extend(end_1of3)
  all_orig.extend(end_2of3)
  all_orig.extend(end_3of3)
  all_orig.extend(data_provider)
  all_orig.extend(system_ID)
  all_orig.extend(installation_date)
  all_orig.extend(system_size_DC)
  all_orig.extend(total_installed_price)
  all_orig.extend(rebate_or_grant)
  all_orig.extend(customer_segment)
  all_orig.extend(expansion_system)
  all_orig.extend(multiple_phase_system)
  all_orig.extend(new_construction)
  all_orig.extend(tracking)
  all_orig.extend(ground_mounted)
  all_orig.extend(zip_code)
  all_orig.extend(city)
  all_orig.extend(state)
  all_orig.extend(utility_service_territory)
  all_orig.extend(third_party_owned)
  all_orig.extend(installer_name)
  all_orig.extend(self_installed)
  all_orig.extend(additional_modules)
  all_orig.extend(additional_inverters)
  all_orig.extend(DC_optimizer)
  all_orig.extend(inverter_loading_ratio)
  all_orig.extend(battery_rated_capacity_kW)
  all_orig.extend(battery_rated_capacity_kWh)
  return all_orig
