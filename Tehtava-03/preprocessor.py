import pandas as pd
import numpy as np
import os
import importlib.util
import sys

# Määritetään tarvittavat polut lähtien projektin juuresta listoina
module_file_path = ["Tehtava-03", "columns_file.py"]
pred_file_path = ['docs', '50-Koneoppiminen', 'scripts', 'installation_count_pred.py']
data_file_path = ['data', 'LBNL_file.csv']
project_root = "solar-panel-data-project"
new_head = os.getcwd(); k = 0  # k vain while-silmukan rajoitusta varten

# Selvitetään kuinka monta tasoa pitää liikkua projektin juurikansiota kohti
while k < 9:
  head, tail = os.path.split(new_head); new_head = head; k += 1
  if tail == project_root:
    break
  module_file_path.insert(0, os.pardir)
  pred_file_path.insert(0, os.pardir)
  data_file_path.insert(0, os.pardir)

try:
  import columns_file as cf
except ModuleNotFoundError:
  file_path = os.curdir
  for name in module_file_path:
    file_path = os.path.join(file_path, name)
  module_name = "columns_file"
  spec = importlib.util.spec_from_file_location(module_name, file_path)
  cf = importlib.util.module_from_spec(spec)
  sys.modules[module_name] = cf
  spec.loader.exec_module(cf)

try:
  import installation_count_pred as tsp
except ModuleNotFoundError:
  file_path = os.curdir
  for name in pred_file_path:
    file_path = os.path.join(file_path, name)
  module_name = "installation_count_pred"
  spec = importlib.util.spec_from_file_location(module_name, file_path)
  tsp = importlib.util.module_from_spec(spec)
  sys.modules[module_name] = tsp
  spec.loader.exec_module(tsp)


# Vakioita, joilla määritetään raja-arvoja esikäsittelijälle
# oletusarvoina
###########################################################
# Oletusmode
default_mode = 'common'

# Tämä määrää hinta/teho -arvon, jonka alittavilta asennuksilta
# karsitaan hintatieto
default_price_per_kW_limit = 1000

# Marginaali määrittää kuinka paljon laskettu järjestelmän koko
# voi suhteellisesti poiketa ilmoitetusta 'system_size_DC':n
# arvosta, jotta esikäsittelijä hyväksyy sen alkuperäisen arvon
default_error_margin = 0.15

# Suuren voimalan tehon alaraja (kW) (customer_segment_3)
default_large_limit = 100

# Hinnoista poistetaan ulkopuolisten omistamat
default_no_tpo_prices = False

# Koko (teho) hyväksytään, jos sitä ei voi laskien tarkistaa
default_unchecked_sizes = False

# Tesla Energyn asennusten lukeminen akkujärjestelmiin
# none: ei lueta mitään mukaan (varmasti virheellinen tapa)
# neutral: ei erityistä kohtelua
# all: luetaan kaikki akkujärjestelmiksi
default_tesla_battery = 'neutral'

# Tulkitaanko solar_storage_hybrid_inverter merkiksi akusta
default_sshi_with_battery = False


# Mode-valikoima
###########################################################
modes = ['none', 'minimal', 'common']


def esik(cols,
    mode = default_mode,
    price_per_kW_limit = default_price_per_kW_limit,
    error_margin = default_error_margin,
    large_limit = default_large_limit,
    no_tpo_prices = default_no_tpo_prices,
    unchecked_sizes = default_unchecked_sizes,
    tesla_battery = default_tesla_battery,
    sshi_with_battery = default_sshi_with_battery
  ):
  if mode in modes:
    df = main(cols[:],
      mode,
      price_per_kW_limit,
      error_margin,
      large_limit,
      no_tpo_prices,
      unchecked_sizes,
      tesla_battery,
      sshi_with_battery
    )
    return df
  else:
    print("Moden valinta on virheellinen. Vaihtoehdot:")
    for item in modes:
      print(item)


###########################################################


def main(cols,
    mode,
    price_per_kW_limit,
    error_margin,
    large_limit,
    no_tpo_prices,
    unchecked_sizes,
    tesla_battery,
    sshi_with_battery
  ):
  cols_extended = select_cols(cols[:])
  extra_cols = get_extra_cols(cols_extended[:])
  usecols = get_usecols(extra_cols[:], cols_extended[:])
  url = get_url()
  df = pd.read_csv(url, usecols=usecols, low_memory=False, na_values = '-9999')

  if mode == 'none':
    if extra_cols:
      cols = get_usecols(extra_cols[:], cols[:])
      print("#" * 60)
      print("Itse määriteltyjä sarakkeita ei ladattu.")
      print("#" * 60)
    return df[cols]

  if 'installation_date' in cols_extended:
    if mode in ['minimal', 'common']:
      df['installation_date'] = pd.to_datetime(df['installation_date'], format='%d-%b-%Y %H:%M:%S')

  if 'year' in cols_extended:
    if mode in ['minimal', 'common']:
      df['year'] = df['installation_date'].dt.year

  if 'system_size_DC' in cols_extended:
    if mode in ['common']:
      # Tehdään laskenta df_temp DataFrame:iin.
      # Korvataan aluksi laskentaan tarvittavien sarakkeiden NaN-arvot nollalla,
      # jotta laskenta onnistuu
      df_temp = df.copy()
      calc_cols = []
      calc_cols.extend(cf.system_size_DC)
      calc_cols.extend(cf.module_quantity)
      calc_cols.extend(cf.nameplate_capacity_module)
      df_temp[calc_cols] = df_temp[calc_cols].fillna(0)

      # Laskentaa ei suoriteta, jos ei olla varmoja siitä, että moduulityyppejä on enintään kolme
      # tai jos 'system_size_DC' arvo puuttuu
      df_temp_1 = df_temp[(df_temp['additional_modules'] != 0) | (df_temp['system_size_DC'] == 0)]
      # Poistetaan em. rivit käsittelystä tuplaamalla ne ensin, jonka jälkeen
      # kaikki tuplatut poistetaan indeksin perusteella (tuskin paras tapa menetellä)
      # Poisto tehdään seuraavan for-silmukan jälkeen
      df_temp = pd.concat([df_temp, df_temp_1])

      # Karsitaan sellaiset rivit, joissa on vain module_quantity tai vain nameplate_capacity_module
      for i in range(len(cf.nameplate_capacity_module)):
        df_temp_1 = df_temp[(df_temp[cf.module_quantity[i]] > 0) & (df_temp[cf.nameplate_capacity_module[i]] == 0)]
        df_temp_2 = df_temp[(df_temp[cf.module_quantity[i]] == 0) & (df_temp[cf.nameplate_capacity_module[i]] > 0)]
        df_temp = pd.concat([df_temp, df_temp_1, df_temp_2])

      #print("df_temp.shape:", df_temp.shape)
      df_temp['df_temp_index'] = df_temp.index
      df_temp.drop_duplicates(subset=['df_temp_index'], keep=False, inplace=True)
      df_temp.drop('df_temp_index', axis=1, inplace=True)

      # Laskenta
      df_temp['calc_size'] = ((df_temp['module_quantity_1'] * df_temp['nameplate_capacity_module_1']) +
                              (df_temp['module_quantity_2'] * df_temp['nameplate_capacity_module_2']) +
                              (df_temp['module_quantity_3'] * df_temp['nameplate_capacity_module_3']))

      # Karsitaan nollat, koska niitä ei voi tarkistaa laskemalla muutenkaan
      calc_cols.append('calc_size')
      df_temp[calc_cols] = df_temp[calc_cols].replace(0, np.nan)

      # Muutos W -> kW
      df_temp['calc_size'] = df_temp['calc_size'] / 1000
      # Suhdeluku osoittaa hyvin onko vastaavuutta
      df_temp['size_ratio'] = df_temp['calc_size'] / df_temp['system_size_DC']

      # Karsinta sallitun virhemarginaalin mukaan
      if unchecked_sizes:
        # Vaihtoehto, jos vain laskennan mukaan poikkeavat poistetaan
        df_temp = df_temp[(df_temp['size_ratio'] < (1 - error_margin)) | (df_temp['size_ratio'] > (1 + error_margin))]
      else:
        # Vaihtoehto, jos vain laskennan mukaan sallitun poikkeaman sisällä hyväksytään
        df_temp = df_temp[(df_temp['size_ratio'] >= (1 - error_margin)) & (df_temp['size_ratio'] <= (1 + error_margin))]

        # Poistetaan df_temp sisältämät rivit df:stä käyttäen jälleen muuttujaa df_temp,
        # jotta jäljelle jää nollattavat rivit
        df_temp = pd.concat([df, df_temp])
        df_temp['df_temp_index'] = df_temp.index
        df_temp.drop_duplicates(subset=['df_temp_index'], keep=False, inplace=True)
        df_temp.drop('df_temp_index', axis=1, inplace=True)
      # Asetetaan nollaksi, päivitetään ne käsiteltyyn df:ään ja muutetaan ne NaN-arvoiksi
      df_temp['system_size_DC'] = 0
      df.update(df_temp['system_size_DC'])
      df['system_size_DC'] = df['system_size_DC'].replace(0, np.nan)

  if 'total_installed_price' in cols_extended:
    if mode in ['common']:
      df_temp = df.copy()

      # Ensimmäinen karsinta: hinta/teho
      df_temp['price_per_kW'] = df['total_installed_price'] / df['system_size_DC']
      df_temp_1 = df_temp[(df_temp['price_per_kW'] < price_per_kW_limit)]

      # Toinen karsinta: ulkopuolinen omistaja (oletuksena ei karsita)
      if no_tpo_prices:
        df_temp_2 = df[(df['third_party_owned'] != 0)]

      # Kolmas karsinta: itse asennettu
      df_temp_3 = df[(df['self_installed'] != 0)]

      # Kooste ja toistuvien poisto
      if no_tpo_prices:
        df_temp = pd.concat([df_temp_1, df_temp_2, df_temp_3])
      else:
        df_temp = pd.concat([df_temp_1, df_temp_3])
      df_temp['df_temp_index'] = df_temp.index
      df_temp.drop_duplicates(subset=['df_temp_index'], inplace=True)
      df_temp.drop('df_temp_index', axis=1, inplace=True)

      # Muutos palautukseen, poistetut NaN-arvoiksi
      df_temp['total_installed_price'] = 0
      df.update(df_temp['total_installed_price'])
      df['total_installed_price'] = df['total_installed_price'].replace(0, np.nan)

  if 'technology_module_1' in cols_extended:
    if mode in ['common']:
      r = {"Multi-c-Si": "Poly",
           "a-Si": "Thin Film",
           "CIGS": "Thin Film",
           'CdTe': 'Thin Film',
           '<undefined>': np.nan}
      df[cf.technology_module] = df[cf.technology_module].replace(r, regex=True)

  if 'battery_storage' in cols_extended:
    if mode in ['minimal', 'common']:
      # Ehdot, joiden perusteella on päätelty akun kuuluminen järjestelmään
      if tesla_battery == 'all':
        df_temp_1 = df[(df[cf.installer_name[0]] == 'Tesla Energy')]
      if sshi_with_battery:
        df_temp_2 = df[(df['solar_storage_hybrid_inverter_1'] == 1)]
        df_temp_3 = df[(df['solar_storage_hybrid_inverter_2'] == 1)]
        df_temp_4 = df[(df['solar_storage_hybrid_inverter_3'] == 1)]
      df_temp_5 = df[(df['battery_rated_capacity_kW'] > 0)]
      df_temp_6 = df[(df['battery_rated_capacity_kWh'] > 0)]

      # Kooste sekä toistuvat pois.
      # Käydään läpi tarvittavat kombinaatiot, jotta määrätään akut
      # oikein annettujen parametrien mukaan.
      if (tesla_battery == 'all') and (sshi_with_battery):
        df_temp = pd.concat([df_temp_1, df_temp_2, df_temp_3, df_temp_4, df_temp_5, df_temp_6])
      elif (tesla_battery == 'all') and (~sshi_with_battery):
        df_temp = pd.concat([df_temp_1, df_temp_5, df_temp_6])
      elif (tesla_battery != 'all') and (sshi_with_battery):
        df_temp = pd.concat([df_temp_2, df_temp_3, df_temp_4, df_temp_5, df_temp_6])
      elif (tesla_battery != 'all') and (~sshi_with_battery):
        df_temp = pd.concat([df_temp_5, df_temp_6])
      else:
        print("interpreter was here")
        print("Tänne ei olisi pitänyt päätyä. Lopetetaan ohjelman suorittaminen.")
        sys.exit(1)
      df_temp['df_temp_index'] = df_temp.index
      df_temp.drop_duplicates(subset=['df_temp_index'], inplace=True)
      df_temp.drop('df_temp_index', axis=1, inplace=True)

      # Akkujärjestelmiin 1, jos akku on, muuten 0
      df_temp['battery_storage'] = 1
      if tesla_battery == 'none':
        # Poistetaan akun merkkaus Tesla Energyn asennuksista (vaikka lisätäänkin virhettä)
        df_temp_1 = df_temp[(df_temp[cf.installer_name[0]] == 'Tesla Energy')]
        df_temp.loc[df_temp_1.index, 'battery_storage'] = 0
      df['battery_storage'] = 0
      df.update(df_temp['battery_storage'])
      df['battery_storage'] = df['battery_storage'].astype('int64')

  if 'customer_segment_2' in cols_extended:
    if mode in ['minimal', 'common']:
      r = {"COM": "NON-RES",
           "NON-PROFIT": "NON-RES",
           "SCHOOL": "NON-RES",
           'GOV': 'NON-RES'}
      df['customer_segment_2'] = df['customer_segment'].replace(r, regex=True)

  if 'customer_segment_3' in cols_extended:
    if mode in ['minimal', 'common']:
      df['customer_segment_3'] = df['customer_segment_2']
      df_temp = df[(df['customer_segment_3'] == 'NON-RES') & (df['system_size_DC'] > large_limit)]
      df.loc[df_temp.index, 'customer_segment_3'] = 'LARGE NON-RES'
      df_temp = df[(df['customer_segment_3'] == 'NON-RES') & (df['system_size_DC'] <= large_limit)]
      df.loc[df_temp.index, 'customer_segment_3'] = 'SMALL NON-RES'
      # Tätä tuskin tarvitsee poistaa
      #df['customer_segment_3'].replace('NON-RES', np.nan, inplace=True)

  if 'price_per_kW' in cols_extended:
    if mode in ['minimal', 'common']:
      df['price_per_kW'] = df['total_installed_price'] / df['system_size_DC']

  if 'price_per_W' in cols_extended:
    if mode in ['minimal', 'common']:
      df['price_per_W'] = df['price_per_kW'] / 1000

  return df[cols]


# Sarakevalinnat ja get_url
###########################################################


def select_cols(cols_extended):
  '''Jos sarakkeen esikäsittely vaatii muiden sarakkeiden lataamista,
  muut sarakkeet on liitettävä varmuuden vuoksi listalle muuttujaan
  "cols_extended".'''

  if 'customer_segment_3' in cols_extended:
    cols_extended.extend(['customer_segment_2', 'system_size_DC'])

  if 'customer_segment_2' in cols_extended:
    cols_extended.extend(cf.customer_segment)

  if 'year' in cols_extended:
    cols_extended.extend(cf.installation_date)

  if 'price_per_W' in cols_extended:
    cols_extended.extend(['price_per_kW'])

  if 'price_per_kW' in cols_extended:
    cols_extended.extend(['total_installed_price', 'system_size_DC'])

  if 'total_installed_price' in cols_extended:
    cols_extended.extend(['system_size_DC', 'self_installed', 'third_party_owned'])

  if 'battery_storage' in cols_extended:
    cols_extended.extend(cf.installer_name)
    cols_extended.extend(cf.solar_storage_hybrid_inverter)
    cols_extended.extend(['battery_rated_capacity_kW', 'battery_rated_capacity_kWh'])
  
  if 'system_size_DC' in cols_extended:
    cols_extended.extend(cf.nameplate_capacity_module)
    cols_extended.extend(cf.module_quantity)
    cols_extended.extend(cf.additional_modules)

  # Drop duplicate column names
  cols_extended = pd.Series(cols_extended).unique().tolist()
  
  return cols_extended


def get_extra_cols(cols):
  '''Poimitaan kyselystä itse määritellyt sarakkeet
  omaan listaansa.'''

  extra_cols = []

  for item in cols:
    if item not in cf.all_orig():
      extra_cols.append(item)
  return extra_cols


def get_usecols(extra_cols, cols):
  '''Erotellaan kyselystä itse määriteltävät sarakkeet,
  jotta niitä ei yritetä ladata lähdetiedostosta.
  Esimerkiksi 'price_per_kW' on määritelty itse.'''

  for item in extra_cols:
    try:
      cols.remove(item)
    except ValueError:
      continue

  return cols


def get_url():
  url = os.curdir
  for name in data_file_path:
    url = os.path.join(url, name)
  return url
